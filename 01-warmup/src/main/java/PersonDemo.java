import java.util.ArrayList;
import java.util.List;

import pretest.Person;

public class PersonDemo {
	public static void main(String[] args) {
		List<Person> inimesed = new ArrayList<>();
		inimesed.add(new Person("Juhan"));
		inimesed.add(new Person("Maris"));
		inimesed.add(new Person("Hannes"));
		inimesed.add(new Person("Teele"));
		inimesed.add(new Person("Peeter"));
		inimesed.get(2).printGreetings();
	}
}