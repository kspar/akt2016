import java.util.Arrays;
import java.util.List;

import pretest.Tree;

public class TreeDemo {
    public static void main(String[] args) {
        Tree tree1 = Tree.node(Tree.leaf(3), Tree.node(Tree.leaf(7), Tree.leaf(2)));
        System.out.println(tree1.toList()); 

        // Proovime nüüd listist moodustada puu
        List<Integer> list = Arrays.asList(8, 19, 20, 200, 120, 9, 10);
        Tree tree2 = Tree.fromList(list);

        // Moodustatud puu peab saama tasandada samaks listiks:
        System.out.println(tree2.toList()); 


        // Boonus: Puude kõrgus võiks ka olla võimalikult madal.
        System.out.println(tree1.height()); 
        System.out.println(tree2.height()); 
    }
}
