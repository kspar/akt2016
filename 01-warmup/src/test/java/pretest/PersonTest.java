package pretest;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class PersonTest {

    @Test
    public void test01_numLines() throws Exception {
        Person p1 = new Person("Juhan");
        Person p2 = new Person("Anna");
        new Person("Peeter");
        String[] lines = captureStdout(() -> p1.printGreetings());
        Assert.assertEquals(2, lines.length);
        
        new Person("Maria");
        lines = captureStdout(() -> p2.printGreetings());
        Assert.assertEquals(3, lines.length);
    }

    @Test
    public void test02_names() throws Exception {
        Person p1 = new Person("Nils");
        String[] lines = captureStdout(() -> p1.printGreetings());
        Assert.assertEquals(4, lines.length);
        Assert.assertEquals("Tere, Juhan!", lines[0].trim());
        Assert.assertEquals("Tere, Maria!", lines[3].trim());
    }

    @Test
    public void test03_dupl() throws Exception {
        Person p1 = new Person("Juhan");
        Person p2 = new Person("Anna");
        String[] lines = captureStdout(() -> p1.printGreetings());
        Assert.assertEquals(6, lines.length);
        Assert.assertEquals("Tere, Juhan!", lines[0].trim());
        Assert.assertEquals("Tere, Maria!", lines[3].trim());
        Assert.assertEquals("Tere, Anna!", lines[5].trim());
        
        new Person("Allan");
        lines = captureStdout(() -> p2.printGreetings());
        Assert.assertEquals(7, lines.length);
        Assert.assertEquals("Tere, Juhan!", lines[0].trim());
        Assert.assertEquals("Tere, Maria!", lines[3].trim());
        Assert.assertEquals("Tere, Juhan!", lines[5].trim());
        Assert.assertEquals("Tere, Allan!", lines[6].trim());
    }
    
    private static String[] captureStdout(Runnable action) {
    	PrintStream originalStdout = System.out;
    	try {
    		ByteArrayOutputStream output = new ByteArrayOutputStream();
    		System.setOut(new PrintStream(output));
    		action.run();
    		return output.toString().split("(\\r)?\\n");
    	} finally {
    		System.setOut(originalStdout);
    	}
    }
}