package pretest;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.number.OrderingComparison.lessThanOrEqualTo;
import static pretest.Tree.node;
import static pretest.Tree.leaf;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class TreeTest {

    private Tree t1 = node(node(leaf(5), leaf(10)), leaf(8));
    private Tree t2 = node(
            node(node(leaf(19), leaf(2)), node(leaf(33), node(leaf(6), leaf(40)))),
            node(node(leaf(-6), leaf(7)), node(leaf(7), node(leaf(93), leaf(10)))));

    @Test
    public void test01_height() {
        Assert.assertEquals(2, t1.height());
        Assert.assertEquals(4, t2.height());
    }

    @Test
    public void test02_toList() {
        Assert.assertEquals(Arrays.asList(5,10,8), t1.toList());
        Assert.assertEquals(Arrays.asList(19, 2, 33, 6, 40, -6, 7, 7, 93, 10), t2.toList());
    }

    @Test
    public void test03_fromList() {
        checkFromList(5);
        checkFromList(1, 3, 59, -3, 20, 100);
        checkFromList(1, 3, 59, -3, 20, 100, 99);
    }

    private void checkFromList(Integer... ints) {
        List<Integer> intList = Arrays.asList(ints);
        Assert.assertEquals(intList, Tree.fromList(intList).toList());
    }

    @Test
    public void test04_fromListBalanced() {
        checkFromListBalanced(5);
        checkFromListBalanced(5, 6);
        checkFromListBalanced(5, 6, 3);
        checkFromListBalanced(1, 3, 59, -3, 20, 100);
        checkFromListBalanced(1, 3, 59, -3, 20, 100, 99);
    }

    private void checkFromListBalanced(Integer... ints) {
        List<Integer> intList = Arrays.asList(ints);
        Tree tree = Tree.fromList(intList);
        Assert.assertEquals(intList, tree.toList());
        Assert.assertThat("Tree is not balanced!", tree.height(), lessThanOrEqualTo(1+log2(intList.size())));
    }

    // http://stackoverflow.com/questions/3305059/how-do-you-calculate-log-base-2-in-java-for-integers
    public static int log2(int n){
        if(n <= 0) throw new IllegalArgumentException();
        return 31 - Integer.numberOfLeadingZeros(n);
    }

}
