package pretest;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class PingPongTest {
    private String[] split;

    @Before
    public void setUpStream() {
        split = captureStdout(() -> PingPong.playPingPong(1,50)); 
    }

    @Test
    public void test01_Ping() throws Exception {
        assertEquals(50, split.length);
        assertEquals("1", split[0]);
        assertEquals("2", split[1]);
        assertEquals("8", split[7]);
        assertEquals("Ping", split[2]);
        assertEquals("Ping", split[11]);
    }

    @Test
    public void test02_Pong() throws Exception {
        assertEquals(50, split.length);
        assertEquals("1", split[0]);
        assertEquals("2", split[1]);
        assertEquals("8", split[7]);
        assertEquals("Pong", split[4]);
        assertEquals("Pong", split[9]);
        assertEquals("Pong", split[49]);
    }

    @Test
    public void test03_PingPong() throws Exception {
        assertEquals(50, split.length);
        assertEquals("1", split[0]);
        assertEquals("2", split[1]);
        assertEquals("8", split[7]);
        assertEquals("Ping", split[2]);
        assertEquals("Ping", split[11]);
        assertEquals("Pong", split[4]);
        assertEquals("Pong", split[9]);
        assertEquals("Pong", split[49]);
        assertEquals("PingPong", split[14]);
    }

    private static String[] captureStdout(Runnable action) {
    	PrintStream originalStdout = System.out;
    	try {
    		ByteArrayOutputStream output = new ByteArrayOutputStream();
    		System.setOut(new PrintStream(output));
    		action.run();
    		return output.toString().split("(\\r)?\\n");
    	} finally {
    		System.setOut(originalStdout);
    	}
    }

}