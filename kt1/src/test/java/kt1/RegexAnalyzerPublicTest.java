package kt1;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ee.ut.cs.akt.regex.RegexParser;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegexAnalyzerPublicTest {

	@Test
	public void test01_finite() {
		check("a", 		1, 1);
		check("ε|ab", 	0, 2);
	}

	@Test
	public void test02_infinite() {
		check("a*", 	0);
		check("aa*", 	1);
	}
	
	private void check(String regex, int expectedMin, int expectedMax) {
		assertEquals(new WordsLengthInfo(expectedMin, expectedMax),
				RegexAnalyzer.getWordsLengthInfo(RegexParser.parse(regex)));
	}
	
	private void check(String regex, int expectedMin) {
		assertEquals(new WordsLengthInfo(expectedMin),
				RegexAnalyzer.getWordsLengthInfo(RegexParser.parse(regex)));
	}

}
