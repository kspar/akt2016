package ee.ut.cs.akt.regex;

public class Repetition extends RegexNode {

    public Repetition(RegexNode child) {
        super('*', child);
    }

    public RegexNode getChild() {
        return getChild(0);
    }

}
