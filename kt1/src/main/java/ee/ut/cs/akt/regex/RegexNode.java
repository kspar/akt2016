package ee.ut.cs.akt.regex;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

/**
 * Regulaaravaldise süntakspuu tipude ülemklass. Annab ka homogeense vaade ASTile.
 */

public abstract class RegexNode {
    public final char type;
    public final List<RegexNode> children;
    

    public RegexNode(char type, RegexNode... children) {
        this.type = type;
        this.children = Arrays.asList(children);
    }

    public String toString() {
        if (children.isEmpty()) return Character.toString(type);
        StringJoiner joiner = new StringJoiner(",", type + "(", ")");
        for (RegexNode child : children) joiner.add(child.toString());
        return joiner.toString();
    }

    public List<RegexNode> getChildren() {
        return children;
    }

    public RegexNode getChild(int i) {
        return children.get(i);
    }

    @Override
    public boolean equals(Object that) {
        return this.toString().equals(that.toString());
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

}


