package ee.ut.cs.akt.regex;

public class Epsilon extends RegexNode {
    public Epsilon() {
        super('ε');
    }
}
