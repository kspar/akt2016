package kt1;

public class WordsLengthInfo {
	private final int minLength;
	private final int maxLength;
	
	/* Kasuta seda konstruktorit lõpliku keele korral */
	public WordsLengthInfo(int minLength, int maxLength) {
		this.minLength = minLength;
		this.maxLength = maxLength;
	}
	
	/* Kasuta seda konstruktorit lõpmatu keele korral */
	public WordsLengthInfo(int minLength) {
		this.minLength = minLength;
		this.maxLength = -1;
	}
	
	boolean hasMaximumLength() {
		return this.maxLength > -1;
	}
	
	int getMinimumLength() {
		return this.minLength;
	}
	
	int getMaximumLength() {
		if (this.maxLength > -1) {
			return this.maxLength;
		}
		else {
			throw new RuntimeException("No maximum length");
		}
	}
	
	@Override
	public String toString() {
		if (this.hasMaximumLength()) {
			return "Lõplik keel, minimaalne sõna pikkus: " + this.minLength + ", maksimaalne: " + this.maxLength;
		} else {
			return "Lõpmatu keel, minimaalne sõna pikkus: " + this.minLength;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof WordsLengthInfo)) {
			return false;
		}
		
		WordsLengthInfo that = (WordsLengthInfo)obj;
		return that.maxLength == this.maxLength && that.minLength == this.minLength;
	}
	
	@Override
	public int hashCode() {
		return Integer.hashCode(this.minLength) + Integer.hashCode(this.maxLength);
	}
}