package pretest;

import java.util.Collections;
import java.util.List;

class TreeLeaf extends Tree {

    protected int value;

    public TreeLeaf(int value) {
        this.value = value;
    }

    @Override
    public int height() {
        return 0;
    }

    @Override
    public List<Integer> toList() {
        return Collections.singletonList(value);
    }

}
