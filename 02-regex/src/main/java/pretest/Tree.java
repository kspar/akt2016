package pretest;

import java.util.Arrays;
import java.util.List;

public abstract class Tree {

    public abstract int height();

    public abstract List<Integer> toList();

    public static Tree node(Tree t1, Tree t2) {
        return new TreeNode(t1, t2);
    }

    public static Tree leaf(int value) {
        return new TreeLeaf(value);
    }

    public static Tree fromList(List<Integer> list) {
        int length = list.size();
        if (length <= 1) return new TreeLeaf(list.get(0));
        int mid = length / 2;
        Tree left = fromList(list.subList(0, mid));
        Tree right = fromList(list.subList(mid, length));
        return new TreeNode(left, right);
    }

    // See on siis lihtsam variant, mis ei ole tasakaalustatud.
    public static Tree fromListSimple(List<Integer> list) {
        Tree result = new TreeLeaf(list.get(0));
        for (int i = 1; i < list.size(); i++) {
            result = new TreeNode(result, new TreeLeaf(list.get(i)));
        }
        return result;
    }

    public static void main(String[] args) {
        Tree tree1 = node(leaf(3), node(leaf(7), leaf(2)));
        System.out.println(tree1.height()); // 2
        System.out.println(tree1.toList()); // [3, 7, 2]

        // Proovime nüüd listist moodustada puu
        List<Integer> list = Arrays.asList(8, 19, 20, 200, 120, 9, 10);
        Tree tree2 = fromList(list);

        // Moodustatud puu peab saama tasandada samaks listiks:
        System.out.println(tree2.toList()); // [8, 19, 20, 200, 120, 9, 10]


        // Optional: Selle puu kõrgus võiks ka olla võimalikult madal.
        System.out.println(tree2.height()); // 3
    }


}
