package pretest;

import java.util.LinkedList;
import java.util.List;

class TreeNode extends Tree {

    private Tree left;
    private Tree right;

    public TreeNode(Tree left, Tree right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int height() {
        return Math.max(left.height(), right.height()) + 1;
    }

    @Override
    public List<Integer> toList() {
        LinkedList<Integer> list = new LinkedList<>();
        list.addAll(left.toList());
        list.addAll(right.toList());
        return list;
    }

}
