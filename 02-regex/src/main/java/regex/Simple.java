package regex;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Simple {

    // https://courses.cs.ut.ee/2016/AKT/spring/Main/Regex

    // sõnad "kalad" või "jalad".
    public static final String RE1 = "";

    // Viietähelisi sõnu, mis lõppevad tähtedega "alad".
    public static final String RE2 = "";

    // color ja colour
    public static final String RE3 = "";

    // jaha, jahaaaaaaaaa!
    public static final String RE4 = "";

    // binaarsõned
    public static final String RE5 = "";

    // eelviimane täht on "a"
    public static final String RE6 = "";

    // tagasiviited
    public static final String RE7 = "";

    // nimede asendamine
    public static final String RE8 = "";
    public static final String RP8 = "";

    // papa
    public static final String RE9 = "";



    // Testimise meetod, et saaks natuke debuugida, muidu on ka automaattestid.

    public static void main(String[] args) throws IOException {
        List<String> strings =  Files.readLines(new File("sisend.txt"), Charsets.UTF_8);
        printLines(Util.grep(RE1, strings));
    }

    private static void printLines(List<String> strings) {
        for (String s : strings) System.out.println(s);
    }

}
