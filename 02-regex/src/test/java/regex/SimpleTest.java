package regex;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class SimpleTest {


    private List<String> strings;

    @Before
    public void setUpInput() throws IOException {
        strings = Files.readLines(new File("sisend.txt"), Charsets.UTF_8);

    }

    @Test
    public void test01() throws Exception {
        check(Simple.RE1,
                "Mull-mull-mull-mull, väiksed kalad",
                "kus on teie väiksed jalad?");
    }

    @Test
    public void test02() throws Exception {
        check(Simple.RE2,
                "Mull-mull-mull-mull, väiksed kalad",
                "kus on teie väiksed jalad?",
                "Aastavahetuse magusad palad");
    }

    @Test
    public void test03() throws Exception {
        check(Simple.RE3,
                "This color is green.",
                "This colour is red.");
    }

    @Test
    public void test04() throws Exception {
        check(Simple.RE4,
                "jahaaaaaaaaaaaaa!",
                "jaha",
                "jaahaa");
    }

    @Test
    public void test05() throws Exception {
        check(Simple.RE5,
                "0100010",
                "011110110",
                "100",
                "",
                "1",
                "101");
    }

    @Test
    public void test06() throws Exception {
        check(Simple.RE6,
                "abab",
                "ababab",
                "baab");
    }

    @Test
    public void test07() throws Exception {
        check(Simple.RE7,
                "abab",
                "baba");
    }

    @Test
    public void test08() throws Exception {
        checkReplace(Simple.RE8, Simple.RP8,
                "Aavik, Johannes",
                "Adamson, Hendrik",
                "Adson, Artur",
                "Afanasjev, Vahur",
                "Alavainu, Ave",
                "Alle, August",
                "Alliksaar, Artur",
                "Alver, Betti",
                "Arder, Ott");
    }


    @Test
    public void test09() throws Exception {
        checkReplace(Simple.RE9, "h",
                "hah");
    }

    private void checkReplace(String re, String rp, String... expected) {
        List<String> result = Util.replace(re, rp, strings);
        assertEquals(Arrays.asList(expected), result);
    }

    private void check(String re, String... expected) {
        List<String> result = Util.grep(re, strings);
        assertEquals(Arrays.asList(expected), result);
    }

}