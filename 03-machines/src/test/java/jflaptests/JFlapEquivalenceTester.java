package jflaptests;

import dk.brics.automaton.Automaton;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Collection;
import java.util.stream.Collectors;

import static org.junit.Assert.fail;

@RunWith(Parameterized.class)
public class JFlapEquivalenceTester {
	public static String lastTestDescription = "";
	
	private File benchmarkFile;

	public JFlapEquivalenceTester(File benchmarkFile) {
		this.benchmarkFile = benchmarkFile;
	}

	@Parameters
	public static Collection<Object[]> data() throws IOException {
        Path path = Paths.get(System.getProperty("user.dir"), "src", "test", "jflap");
        PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher("glob:**.jff");
		return Files.walk(path).sorted().filter(pathMatcher::matches).map(p -> new Object[]{p.toFile()}).collect(Collectors.toList());
	}

	@Test
	public void test() {
		Automaton benchAutomaton = BricsUtils.fromJFlap(this.benchmarkFile);
        String fileName = benchmarkFile.getName().replace("bench", "yl");
        String fileDir = benchmarkFile.getParentFile().getName();

        File testableFile = Paths.get(System.getProperty("user.dir"), "src", "main", "jflap", fileDir, fileName).toFile();


        lastTestDescription = Paths.get(fileDir, testableFile.getName()).toString();
				
				
		if (testableFile.exists()) {
			Automaton testableAutomaton = BricsUtils.fromJFlap(testableFile);
			if (!testableAutomaton.equals(benchAutomaton)) {
				Automaton diff = benchAutomaton.minus(testableAutomaton);
				
				if (diff.isEmpty()) {
					diff = testableAutomaton.minus(benchAutomaton);
				}
				
				fail(String.format("Esitatud automaat (%s) ei anna õiget vastust sisendiga '%s'",
                        lastTestDescription, diff.getShortestExample(true)));
			}
		} else {
			fail("Ei leia lahenduse faili: " + testableFile);
		}
	}

}
