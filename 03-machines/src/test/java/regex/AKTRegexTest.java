package regex;

import dk.brics.automaton.Automaton;
import dk.brics.automaton.BasicOperations;
import dk.brics.automaton.RegExp;
import org.junit.Test;

import static org.junit.Assert.fail;

public class AKTRegexTest {


    @Test
    public void testRegex1() throws Exception {
        check(Machines.getA1(), AKTRegexChallenge.RE1);
    }

    @Test
    public void testRegex2() throws Exception {
        check(Machines.getA2(), AKTRegexChallenge.RE2);
    }

    @Test
    public void testRegex3() throws Exception {
        check(Machines.getA3(), AKTRegexChallenge.RE3);
    }

    @Test
    public void testRegex4() throws Exception {
        check(Machines.getA4(), AKTRegexChallenge.RE4);
    }


    public static void check(Automaton a, String s) {
        Automaton q = (new RegExp(s)).toAutomaton();
        String s1 = BasicOperations.minus(a, q).getShortestExample(true);
        if (s1 != null) fail("Sinu regex ei tunne ära sõna \"" + eps(s1) + "\".");
        String s2 = BasicOperations.minus(q, a).getShortestExample(true);
        if (s2 != null) fail("Sinu regex sobitub ka sõnaga \"" + eps(s2) + "\".");
    }

    private static String eps(String s) {
        if (s.equals("")) return "ε";
        else return s;
    }

}
