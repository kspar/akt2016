package regex;

import dk.brics.automaton.*;

import static dk.brics.automaton.Automaton.makeChar;
import static dk.brics.automaton.Automaton.makeStringMatcher;
import static dk.brics.automaton.Automaton.makeCharSet;
import static dk.brics.automaton.BasicOperations.repeat;
import static dk.brics.automaton.ShuffleOperations.shuffle;

public class Machines {
    private static Automaton a1;
    private static Automaton a2;
    private static Automaton a3;
    private static Automaton a4;

    public static Automaton getA1() {
        if (a1 == null) a1 = shuffle(makeChar('1'), repeat(makeChar('0'), 1));
        return a1;
    }

    public static Automaton getA2() {
        if (a2 == null) a2 = shuffle(paaris('a'), paaris('b'));
        return a2;
    }

    public static Automaton getA3() {
        if (a3 == null) a3 = makeStringMatcher("101").complement().intersection(makeCharSet("01").repeat());
        return a3;
    }

    public static Automaton getA4() {
        if (a4 == null) a4 = makeMod(0,3);
        return a4;
    }

    private static Automaton makeMod(int r, int m) {
        Automaton automaton = new Automaton();
        State[] states = new State[m];
        for (int i = 0; i < states.length; i++)
            states[i] = new State();
        for (int i = 0; i < m; i++) {
            State s = states[i];
            s.addTransition(new Transition('0', states[ (2*i)   % m ]));
            s.addTransition(new Transition('1', states[ (2*i+1) % m ]));
        }
        states[r].setAccept(true);
        State init = new State();
        init.addTransition(new Transition('0', states[0]));
        init.addTransition(new Transition('1', states[1]));
        automaton.setInitialState(init);
        return automaton;
    }

    private static Automaton paaris(char c) {
        return repeat(repeat(makeChar(c), 2, 2));
    }

}
