package kodu1;

import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runners.MethodSorters;

import java.io.*;
import java.lang.ProcessBuilder.Redirect;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AKTK01Test {
	@ClassRule public static Timeout classTimeout = new Timeout(30, TimeUnit.SECONDS);
	@Rule public Timeout timeout = new Timeout(5, TimeUnit.SECONDS);

	public static String lastTestDescription = "";
	
	public static final String DEFAULT_FILENAME = "test_program_for_junit.aktk";

	@Test
    public void test01_basicPrint() throws IOException {
    	check("print 3", "3");
    	check("print 7", "7", File.createTempFile("test_", ".aktk").getAbsolutePath());
    }
	
	@Test
    public void test02_basicPrintAbsoluteFilename() throws IOException {
    	check("print 3", "3");
    	check("print 8753", "8753");
    }
    
	@Test
    public void test03_singlePlusOrSingleMinus() throws IOException {
    	check("print 3 + 2", "5");
    	check("print 14+0", "14");
    	check("print 103 + 5", "108");
    	check("print 103+5", "108");
    	check("print 103 +  5", "108");
    	check("print 30 - 2", "28");
    	check("print 3 - 15", "-12");
    	check("print 3-15", "-12");
    	check("print             3-           15   ", "-12");
    }
    
	@Test
    public void test04_severalPrints() throws IOException {
    	check("print 3\n"
    			+ "print 4\n", "3\n4");
    }
    
	@Test
    public void test05_plusAndMinus() throws IOException {
    	check("print 3 + 2 - 40", "-35");
    	check("print 1+2+3+4+5+6+7+8+9+10+11-11-10-9-8-7-6-5-4", "6");
    }
    
	@Test
    public void test06_simpleVariables() throws IOException {
    	check(    "x=34\n" 
    			+ "print x", "34");
    	
    	check(    "p=34\n"
    			+ "print p", "34");
    	
    	check(    "x=34\n" 
    			+ "p=4\n"
    			+ "print p\n"
    			+ "print x", "4\n34");
    }
    
	@Test
    public void test07_variablesAndExpressions() throws IOException {
    	check(    "x=34\n" 
    			+ "p=x-x+1\n"
    			+ "print p", "1");
    	
    	check(    "x=34\n" 
    			+ "p=x-x+1\n"
    			+ "print p + p + x", "36");
    }
    
	@Test
    public void test08_commentsAndEmptyLines() throws IOException {
    	check(    "x=34\n" 
    			+ "p=x-x+1\n"
    			+ "\n"
    			+ "# kommentaar\n"
    			+ "\n"
    			+ "# kommentaar\n"
    			+ "print p", "1");
    	
    	check(    "x=34\n" 
    			+ "p=x-x+1 # kommentaar\n"
    			+ "print p + p + x # kommentaar", "36");
    }
    
	@Test
    public void test09_undefinedVariables() throws IOException {
    	checkError("print x");
    	checkError("print w");
    	checkError("print x\n"
    			+  "x = 3");
    }
	
	@Test
    public void test10_syntaxErrors() throws IOException {
    	checkError("kala=66");
    	checkError("x=6.6");
    	checkError("x=");
    	checkError("x=3+");
    	checkError("x=3-");
    	checkError("print");
    	checkError("print 3+");
    	checkError("print 3-");
    	checkError("prnt 3");
    }
	
    private void checkError(String program) throws IOException {
		lastTestDescription = "Programm: \n>" + quoteTextBlock(program);
    	File f = createFile(DEFAULT_FILENAME, program);
    	try {
    		ExecutionResult result = runJavaProgramAsSeparateProcess("kodu1.AKTKi", DEFAULT_FILENAME);
    		// stderr voos peaks olema veateade
    		if (result.err.isEmpty()) {
    			fail("Ootasin veateadet, aga seda polnud. Väljund oli " + result.out);
    		}
    	} 
    	finally {
    		f.delete();
    	}
		lastTestDescription = "";
    }
    
    private void check(String program, String expectedOutput) throws IOException {
    	check(program, expectedOutput, DEFAULT_FILENAME);
    }
        
    private void check(String program, String expectedOutput, String filename) throws IOException {
		lastTestDescription = "Programm: \n>" + quoteTextBlock(program);
		
    	File f = createFile(filename, program);
    	try {
    		ExecutionResult result = runJavaProgramAsSeparateProcess("kodu1.AKTKi", filename);

    		// Väljundi kontrollimisel pean arvestama, et Windowsi reavahetus on \r\n, aga mujal on \n.
    		// Samuti tahan ma olla paindlik selle suhtes, kas väljundi lõpus on reavahetus või mitte.
    		expectedOutput = expectedOutput.replace("\r\n", "\n").replaceFirst("\\n$", "");
    		String actualOutput = result.out.replace("\r\n", "\n").replaceFirst("\\n$", "");
    		
    		if (!expectedOutput.equals(actualOutput)) {
    			fail("Ootasin väljundit\n" + quoteTextBlock(expectedOutput) 
    				+ ", aga väljund oli\n" + quoteTextBlock(actualOutput));
    		}
    		assertEquals(expectedOutput, actualOutput);
    		
    		// stderr voog peaks olema tühi, st. programm ei tohiks anda vigu
    		assertTrue(result.err.isEmpty());
    	} 
    	finally {
    		f.delete();
    	}
		lastTestDescription = "";
    }
    
    private String quoteTextBlock(String s) {
    	return "\n>" + s.replace("\n", "\n>") + "\n";
    }
    
	static File createFile(String name, String content) throws IOException{
    	File f = new File(name);
    	PrintWriter fw = new PrintWriter(f, "UTF-8");
    	fw.write(content);
    	fw.close();
    	return f;
    }
    
	public static ExecutionResult runJavaProgramAsSeparateProcess(String className, String... args) throws IOException {
		return runJavaProgramWithInput(className, "", args);
	}
	
	public static ExecutionResult runJavaProgramWithInput(String className, String input, String... args) throws IOException {
	
		List<String> cmdParts = new ArrayList<>();
		cmdParts.add("java");
		cmdParts.add("-mx128m");
		cmdParts.add("-Dfile.encoding=UTF-8");
		cmdParts.add("-cp");
        String gradlePath = Paths.get("build", "classes", "main").toString();
        String eclipsePath = "bin";
        cmdParts.add(joinClasspath(".", eclipsePath, gradlePath));
        cmdParts.add(className);
		cmdParts.addAll(Arrays.asList(args));
	
		// valmista ette sisendi fail
		File inputFile = File.createTempFile("test_input_", "");
		FileOutputStream out = new FileOutputStream(inputFile); 
		out.write(input.getBytes("UTF-8"));
		out.close();

        ProcessBuilder pb = new ProcessBuilder(cmdParts.toArray(new String[cmdParts.size()]));
		pb.redirectInput(inputFile);
		pb.redirectOutput(Redirect.PIPE);
		pb.redirectError(Redirect.PIPE);
		
		Process proc = pb.start();
		
		if (!input.isEmpty()) {
			proc.getOutputStream().write(input.getBytes(Charset.forName("UTF-8")));
		}
		try {
			int returnCode = proc.waitFor();
			return new ExecutionResult(returnCode, 
					readAllFromStream(proc.getInputStream()),
					readAllFromStream(proc.getErrorStream()));
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static String readAllFromStream(InputStream stream) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(stream));
		char[] cbuf = new char[]{1024};
		StringBuilder sb = new StringBuilder();
		while (br.read(cbuf) > -1) {
			sb.append(String.valueOf(cbuf));
		}
		return sb.toString();
	}

	static String joinClasspath(String... parts) {
        return String.join(File.pathSeparator, parts);
	}

    public static class ExecutionResult {
    	public ExecutionResult(int returnCode, String out, String err) {
    		this.out = out;
    		this.err = err;
    		this.returnCode = returnCode;
    	}
    	public String out;
    	public String err;
    	public int returnCode;

        @Override
        public String toString() {
			return String.format("ExecutionResult{out='%s', err='%s', returnCode=%d}", out, err, returnCode);
        }
    }

}
