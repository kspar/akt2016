package htmlstrip;

import java.util.function.Consumer;

// OPTIONAL: See on lihtsalt näide State Design Patterni kasutamise kohta.
public class StatePattern {

    private static Consumer<Character> state;
    private static StringBuilder sb;

    final static Consumer<Character> INI = c -> {
        if (c == '<') state = StatePattern.TAG;
        else sb.append(c);
    };

    final static Consumer<Character> TAG = c -> {
        if (c == '>') state = StatePattern.INI;
        if (c == '\'') state = StatePattern.QTE;
    };

    final static Consumer<Character> QTE = c -> {
        if (c == '\'') state = StatePattern.TAG;
    };

    public static String removeHtmlMarkup(String s) {
        state = INI;
        sb = new StringBuilder();
        for (char c : s.toCharArray()) state.accept(c);
        return sb.toString();
    }
}