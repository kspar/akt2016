package kodu1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class AKTKi {

    public static List<String> tokenize(String input) {
        List<String> tokens = new ArrayList<>();
        String currentToken = null;
        for (char c : input.toCharArray()) {
            // Siia vaja natuke täiendada...
            if (c == '+' || c == '-' || c == ' ') {
                if (currentToken != null) {
                    tokens.add(currentToken);
                    currentToken = null;
                }
                if (c != ' ') tokens.add(Character.toString(c));
            } else {
                if (currentToken != null) currentToken += c;
                else currentToken = Character.toString(c);
            }
        }
        if (currentToken != null) tokens.add(currentToken);
        return tokens;
    }

    public static int compute(List<String> tokens, Map<String, Integer> env) {
        int sign = 1;
        int sum = 0;
        for (String token : tokens) {
            if (token.equals("+")) continue;
            if (token.equals("-")) sign *= -1;
            else sum += sign * getValue(token, env);
        }
        return sum;
    }

    private static int getValue(String token, Map<String, Integer> env) {
        try {
            return Integer.parseInt(token);
        } catch (NumberFormatException e) {
            return env.get(token);
        }
    }

    public static void main(String[] args) throws IOException {
        Map<String, Integer> env = new HashMap<>();
        try(BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
            for(String line; (line = br.readLine()) != null; ) {
                List<String> tokens = tokenize(line);
                if (tokens.size() < 1) continue;
                if (tokens.get(0).equals("print")) System.out.println(compute(tokens.subList(1, tokens.size()), env));
                if (tokens.get(1).equals("=")) env.put(tokens.get(0), compute(tokens.subList(2, tokens.size()), env));
            }
        }
    }

}
