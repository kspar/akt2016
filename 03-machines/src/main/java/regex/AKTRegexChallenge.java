package regex;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class AKTRegexChallenge {

    // NB! Epsiloni asemel tuleb kasutada tühjad sulud: ()
    // Aga küsimärgi ja tärni kasutamisel ei ole epsilonit siin vaja.

    // Tähestiku {0,1} sõnad, milles esinevad täpselt üks '1' ja vähemalt üks '0'.
    public static final String RE1 = "";

    // Tähestiku {a,b} sõnad, mis sisaldavad paarisarv a-sid ja paarisarv b-sid.
    public static final String RE2 = "";

    // Tähestiku {0,1} Sõnad, mis ei sisalda 101.
    public static final String RE3 = "";

    // Kolmega jaguvad binaararvud.
    public static final String RE4 = "";

}

