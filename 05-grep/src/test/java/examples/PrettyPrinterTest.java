package examples;

import ee.ut.cs.akt.regex.RegexNode;
import ee.ut.cs.akt.regex.RegexParser;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrettyPrinterTest {

    @Test
    public void testFormatRegex() throws Exception {
        checkRegex("a");
    }

    @Test
    public void testFormatRegexBasics() throws Exception {
        checkRegex("a");
        checkRegex("abc");
        checkRegex("a|ε");
        checkRegex("(ab)*");
    }

    @Test
    public void testFormatRegexDiff() throws Exception {
        checkRegex("a");
        checkRegex("a*");
        checkRegex("abc");
        checkRegex("a|ε");
        checkRegex("(ab)*");
        checkRegex("(ab*)*");
        checkRegex("(b*)*");
        checkRegex("ab*cd*");
        checkRegex("(a|b)cd");
        checkRegex("a(b|c)d");
        checkRegex("ab(c|d)");
        checkRegex("a(b|c)d");
        checkRegex("a|b|c");
    }


    private void checkRegex(String input) {
        RegexNode node = RegexParser.parse(input);
        assertEquals(input, PrettyPrinter.formatRegex(node));
    }
}