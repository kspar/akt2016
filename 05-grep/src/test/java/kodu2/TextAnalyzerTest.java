package kodu2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TextAnalyzerTest {
    public static String lastTestDescription = "";

    @Test
    public void test00_demoCase() {
        TextAnalyzer ta = new TextAnalyzer(
                "Mina olen Kalle Kulbok ja mu telefoninumber on 5556 4272.\n"
                        + "Mina olen Peeter Peet ja mu telefoninumber on 5234 567.\n"
                        + "Mari Maasikas siin, mu number on 6723 3434.\n"
                        + "Tere, olen Jaan Jubin numbriga 45631643.\n");

        String peetriNr = ta.getPhoneNumbers().get("Peeter Peet");
        String jaaniNr = ta.getPhoneNumbers().get("Jaan Jubin");

        assertEquals("5234567", peetriNr);
        assertEquals("45631643", jaaniNr);
        assertEquals("Mina olen <nimi> ja mu telefoninumber on <telefoninumber>.\n" +
                        "Mina olen <nimi> ja mu telefoninumber on <telefoninumber>.\n" +
                        "<nimi> siin, mu number on <telefoninumber>.\n" +
                        "Tere, olen <nimi> numbriga <telefoninumber>.\n",
                ta.anonymize());
    }

    @Test
    public void test01_getPhoneNumbers() {
        checkPhoneNumber("Mart Laar, tel: 42312345, e-mail:laar@laar.ee", "Mart Laar", "42312345");
        checkPhoneNumber("Suvaline teks ja *+*asf,, Jaak Saar veel miskit +afV+++*., 1234 4444.", "Jaak Saar", "12344444");
    }

    @Test
    public void test02_anonymize() {
        checkAnon("Mart Laar, tel: 42312345, e-mail:laar@laar.ee", "<nimi>, tel: <telefoninumber>, e-mail:laar@laar.ee");
        checkAnon("Suvaline teks ja *+*asf,, Jaak Saar veel miskit +afV+++*., 1234 4444.", "Suvaline teks ja *+*asf,, <nimi> veel miskit +afV+++*., <telefoninumber>.");
    }

    @Test
    public void test03_misc() {
        checkAnon("Mart Laar   42312345", "<nimi>   <telefoninumber>");
        checkAnon("Ma La   42312345.", "<nimi>   <telefoninumber>.");
        checkPhoneNumber("Ma La   42312345.", "Ma La", "42312345");
        checkPhoneNumber("Ma La   423 2345.", "Ma La", "4232345");
    }


    private void checkPhoneNumber(String input, String nimi, String expected) {

        lastTestDescription = "Sisend: \n>" + input.replace("\r\n", "\n").replace("\n", "\n>");

        TextAnalyzer ta = new TextAnalyzer(input);
        String actual = ta.getPhoneNumbers().get(nimi);

        assertEquals(actual + " pole õigel kujul", expected, actual);
    }

    private void checkAnon(String input, String expected) {
        lastTestDescription = "Sisend: \n>" + input.replace("\r\n", "\n").replace("\n", "\n>");

        TextAnalyzer ta = new TextAnalyzer(input);
        String actual = ta.anonymize();

        assertEquals(actual + " pole õigel kujul", expected, actual);
    }
}
