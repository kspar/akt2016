package kodu2;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextAnalyzer {
    private static final String NAME_GROUP = "(\\p{Upper}\\p{Lower}*\\s*\\p{Upper}\\p{Lower}*)";
    private static final String NUMBER_GROUP = "(\\d{3,4}\\s\\d{3,4}|\\d{4,8})";
    private String text;

    public TextAnalyzer(String text) {
        this.text = text;
    }

    public HashMap<String, String> getPhoneNumbers() {
        Matcher matcher = Pattern.compile(NAME_GROUP + ".*?" + NUMBER_GROUP + ".*?\\.").matcher(text);
        HashMap<String, String> phonebook = new HashMap<>();
        while (matcher.find()) phonebook.put(matcher.group(1), matcher.group(2).replaceAll("\\s+", ""));
        return phonebook;
    }

    public String anonymize() {
        return text.replaceAll(NAME_GROUP, "<nimi>").replaceAll(NUMBER_GROUP, "<telefoninumber>");
    }
}
