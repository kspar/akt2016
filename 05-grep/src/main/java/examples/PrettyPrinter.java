package examples;

import ee.ut.cs.akt.regex.*;

public class PrettyPrinter {

    // Tuleb teisendada regex sõneks niimoodi, et on ainult minimaalselt sulgusid.
    public static String formatRegex(RegexNode regex) {
        return regex.toString();
    }
}
