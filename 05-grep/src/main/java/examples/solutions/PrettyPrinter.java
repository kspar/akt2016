package examples.solutions;

import ee.ut.cs.akt.regex.*;

public class PrettyPrinter {

    public static String formatRegex(RegexNode regex) {
        return formatRegex(regex, 0);
    }

    private static String formatRegex(RegexNode regex, int contextPriority) {
        String output = null;
        int priority = priorityOf(regex);

        if (regex instanceof Concatenation) {
            Concatenation concat = (Concatenation) regex;
            output =  formatRegex(concat.getLeft(), priority) + formatRegex(concat.getRight(), priority);
        } else if (regex instanceof Alternation) {
            Alternation alt = (Alternation) regex;
            output =  formatRegex(alt.getLeft(), priority) + '|' + formatRegex(alt.getRight(), priority);
        } else if (regex instanceof Repetition) {
            Repetition rep = (Repetition) regex;
            // NB! Kuna repetition on tavaks väljastada "a**" asemel "(a*)*", peab konteksti prioriteeti siin tõstma.
            output = formatRegex(rep.getChild(), priority+1) + '*';
        } else if (regex instanceof Letter || regex instanceof Epsilon) {
            output = regex.toString();
        }

        if (priority < contextPriority) output = '(' + output + ')';
        return output;
    }

    private static int priorityOf(RegexNode regex) {
        if (regex instanceof Alternation) return 1;
        if (regex instanceof Concatenation) return 2;
        if (regex instanceof Repetition) return 3;
        return 4;
    }
}
