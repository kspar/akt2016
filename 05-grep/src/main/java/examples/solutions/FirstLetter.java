package examples.solutions;

import ee.ut.cs.akt.regex.*;

import java.util.HashSet;
import java.util.Set;


public class FirstLetter {
    public static Set<Character> getFirst(String regex) {
        RegexNode node = RegexParser.parse(regex);
        return getFirst(node).chars;
    }

    private static ResultType getFirst(RegexNode node) {
        ResultType result = new ResultType();

        if (node instanceof Epsilon) {
            result.emptiness = true;
        } else if (node instanceof Letter) {
            Letter letter = (Letter) node;
            result.chars.add(letter.getSymbol());
            result.emptiness = false;
        } else if (node instanceof Alternation) {
            Alternation alt = (Alternation) node;
            ResultType left = getFirst(alt.getLeft());
            ResultType right = getFirst(alt.getRight());
            result.chars.addAll(left.chars);
            result.chars.addAll(right.chars);
            result.emptiness = left.emptiness || right.emptiness;
        } else if (node instanceof Concatenation) {
            Concatenation con = (Concatenation) node;
            ResultType left = getFirst(con.getLeft());
            ResultType right = getFirst(con.getRight());
            result.chars.addAll(left.chars);
            // Siin on see oluline koht, kus kontrollime tühjust:
            if (left.emptiness) result.chars.addAll(right.chars);
            result.emptiness = left.emptiness && right.emptiness;
        }  else if (node instanceof Repetition) {
            Repetition rep = (Repetition) node;
            result.chars.addAll(getFirst(rep.getChild()).chars);
            result.emptiness = true;
        }
        return result;
    }

}

class ResultType {
    public Set<Character> chars = new HashSet<>();
    public boolean emptiness;
}
